'use strict';

var assert = require('chai').assert;
var Contextify = require('contextify');
var sandbox = Contextify();

var Blockly = require('../index.js');

Blockly.JavaScript.LOOP_TRAP_COUNT = 2;

function xmlToJs(xml) {
  try {
    var xml = Blockly.Xml.textToDom(xml);
  }
  catch (e) {
    console.log(e);
    return ''
  }

  var workspace = new Blockly.Workspace();
  Blockly.Xml.domToWorkspace(workspace, xml);
  return Blockly.JavaScript.workspaceToCode(workspace);
}

describe('Loop trap', function() {
  it('should add loop trap', function() {
    var code = xmlToJs('<xml xmlns="http://www.w3.org/1999/xhtml"><block type="variables_set" id="481" x="13" y="13"><field name="VAR">a</field><value name="VALUE"><block type="math_number" id="500"><field name="NUM">0</field></block></value><next><block type="variables_set" id="549"><field name="VAR">b</field><value name="VALUE"><block type="math_number" id="550"><field name="NUM">0</field></block></value><next><block type="variables_set" id="579"><field name="VAR">c</field><value name="VALUE"><block type="math_number" id="593"><field name="NUM">0</field></block></value><next><block type="variables_set" id="592"><field name="VAR">d</field><value name="VALUE"><block type="math_number" id="594"><field name="NUM">0</field></block></value><next><block type="controls_repeat_ext" id="369"><value name="TIMES"><block type="math_number" id="370"><field name="NUM">100000</field></block></value><statement name="DO"><block type="variables_set" id="612"><field name="VAR">a</field><value name="VALUE"><block type="math_arithmetic" id="676"><field name="OP">ADD</field><value name="A"><block type="variables_get" id="706"><field name="VAR">a</field></block></value><value name="B"><block type="math_number" id="707"><field name="NUM">1</field></block></value></block></value><next><block type="controls_whileUntil" id="371"><field name="MODE">WHILE</field><value name="BOOL"><block type="logic_boolean" id="372"><field name="BOOL">TRUE</field></block></value><statement name="DO"><block type="variables_set" id="708"><field name="VAR">b</field><value name="VALUE"><block type="math_arithmetic" id="709"><field name="OP">ADD</field><value name="A"><block type="variables_get" id="710"><field name="VAR">b</field></block></value><value name="B"><block type="math_number" id="711"><field name="NUM">1</field></block></value></block></value><next><block type="controls_for" id="373"><field name="VAR">i</field><value name="FROM"><block type="math_number" id="374"><field name="NUM">1</field></block></value><value name="TO"><block type="math_number" id="375"><field name="NUM">100000</field></block></value><value name="BY"><block type="math_number" id="376"><field name="NUM">1</field></block></value><statement name="DO"><block type="variables_set" id="712"><field name="VAR">c</field><value name="VALUE"><block type="math_arithmetic" id="713"><field name="OP">ADD</field><value name="A"><block type="variables_get" id="714"><field name="VAR">c</field></block></value><value name="B"><block type="math_number" id="715"><field name="NUM">1</field></block></value></block></value><next><block type="controls_forEach" id="377"><field name="VAR">j</field><value name="LIST"><block type="lists_create_with" id="378"><mutation items="4"></mutation></block></value><statement name="DO"><block type="variables_set" id="716"><field name="VAR">d</field><value name="VALUE"><block type="math_arithmetic" id="717"><field name="OP">ADD</field><value name="A"><block type="variables_get" id="718"><field name="VAR">d</field></block></value><value name="B"><block type="math_number" id="719"><field name="NUM">1</field></block></value></block></value></block></statement></block></next></block></statement></block></next></block></statement></block></next></block></statement></block></next></block></next></block></next></block></next></block><block type="variables_get" id="691" x="24" y="65"><field name="VAR">item</field></block></xml>');

    sandbox.run(code);

    assert.equal(sandbox.a, 3)
    assert.equal(sandbox.b, Math.pow(3, 2))
    assert.equal(sandbox.c, Math.pow(3, 3))
    assert.equal(sandbox.d, Math.pow(3, 4))

    sandbox.dispose();

  });
});



